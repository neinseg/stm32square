
BUILDDIR ?= build

WGET ?= wget
PYTHON ?= python3

.PRECIOUS: $(BUILDDIR)/%.jar
.PRECIOUS: $(BUILDDIR)/%.jar.version
$(BUILDDIR)/%.jar:
	mkdir -p $(BUILDDIR)
	$(WGET) --tries=10 -O $@ $$($(PYTHON) tools/get_p2_url.py --write-version $@.version $*)
	touch $@

OUTPUTS := svd
OUTPUTS += mx_mcu_db
OUTPUTS += chip_db
OUTPUTS += ld_memmap
OUTPUTS += buildinfo
OUTPUTS += die_id_table
# OUTPUTS += prog_db 

all: $(OUTPUTS)

$(BUILDDIR)/%.dir: $(BUILDDIR)/%.jar
	mkdir -p $@
	rm -rf $@
	unzip -q -d $@ $<

svd: $(BUILDDIR)/com.st.stm32cube.ide.mcu.productdb.debug.dir
	rm -rf $@
	mkdir -p $@
	cp $</about.html $@/License.html
	cp $</resources/cmsis/STMicroelectronics_CMSIS_SVD/*.svd $@/
	cp $(BUILDDIR)/com.st.stm32cube.ide.mcu.productdb.debug.jar.version $@/source_file_versions.txt

mx_mcu_db: $(BUILDDIR)/com.st.stm32cube.common.mx.dir
	rm -rf $@
	mkdir -p $@
	cp $</about.html $@/License.html
	cp -r $</db/mcu/* $@/
	cp $(BUILDDIR)/com.st.stm32cube.common.mx.jar.version $@/source_file_versions.txt

die_id_table: mx_mcu_db
	grep -o "DIE..." mx_mcu_db/STM* | sed -e "s/\(.*\).xml:\(DIE.*\)/\2  \1/" | sort > die_id_table

#prog_db: $(BUILDDIR)/com.st.stm32cube.ide.mcu.externaltools.cubeprogrammer.linux64.dir
#	rm -rf $@
#	mkdir -p $@
#	cp $</about.html $@/License.html
#	cp $</tools/Data_Base/*.xml $@/
#	cp $(BUILDDIR)/com.st.stm32cube.ide.mcu.externaltools.cubeprogrammer.linux64.jar.version $@/source_file_versions.txt

$(BUILDDIR)/STM32CubeMX.unpacked: build/com.st.stm32cube.common.mx.dir
	unzip -q -d $@ $</STM32CubeMX.jar -x '/*'

chip_db: $(BUILDDIR)/STM32CubeMX.unpacked
	rm -rf $@
	mkdir -p $@
	cp $</LICENSE.txt $@/
	cp $</devices/* $@/
	cp $(BUILDDIR)/com.st.stm32cube.common.mx.jar.version $@/source_file_versions.txt

ld_memmap: chip_db
	rm -rf $@
	$(PYTHON) tools/memmap_gen.py -o $@ chip_db/STM32*.db

buildinfo: chip_db
	rm -rf $@
	$(PYTHON) tools/extract_buildinfo.py -o $@ chip_db/STM32*.db

.PHONY: clean
clean:
	rm -rf svd
	rm -rf mx_mcu_db
#	rm -rf prog_db
	rm -rf chip_db
	rm -rf ld_memmap
	rm -rf buildinfo
	rm -rf build
	rm -f die_id_table

# The following file contains garbage data on the µC's memories.
#stm32targets.xml: $(BUILDDIR)/com.st.stm32cube.ide.mcu.productdb.dir
#	cp $</resources/board_def/$@ $@
#$(BUILDDIR)/com.st.stm32cube.ide.mcu.productdb.dir:
