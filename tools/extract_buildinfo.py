#!/usr/bin/env python3

import utils

def extract_defines(device, f):
    for entry in device.find_all('define'):
        print(entry.string, file=f)

def extract_includes(device, f):
    for entry in device.find_all('include'):
        print(entry.string, file=f)

if __name__ == '__main__':
    utils.generate_chip_files({'defines': extract_defines,
                               'includes': extract_includes})

