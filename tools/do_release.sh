#!/bin/sh
set -e

RELEASE_TRIGGER=${RELEASE_TRIGGER:-"Manual command-line makefile invocation"}

export GIT_AUTHOR_NAME=${GIT_AUTHOR_NAME:-'STM32Square Auto-Release Script'}
export GIT_AUTHOR_EMAIL=${GIT_AUTHOR_EMAIL:-'autorelease@stm32square.jaseg.de'}

if [ ! -z ${GITLAB_USER_EMAIL+x} ]; then
    export GIT_COMMITTER_NAME="$GITLAB_USER_NAME"
    export GIT_COMMITTER_EMAIL="$GITLAB_USER_EMAIL"
fi

TIMESTAMP=${TIMESTAMP:-$(date +%y-%m-%d)}

#git pull -X theirs --no-commit main

make clean all
git add svd mx_mcu_db chip_db ld_memmap buildinfo die_id_table
git commit -q --allow-empty -m "STM32Square Auto-Release $TIMESTAMP

This is an automatic release commit. Generation was invoked from:
$RELEASE_TRIGGER

Component versions:

$(cat build/com.st.stm32cube.ide.mcu.productdb.debug.jar.version)
SHA256: $(sha256sum build/com.st.stm32cube.ide.mcu.productdb.debug.jar)

$(cat build/com.st.stm32cube.common.mx.jar.version)
SHA256: $(sha256sum build/com.st.stm32cube.common.mx.jar)"

get_version () {
    grep "$1 version:" build/$2.jar.version|cut -d: -f2|egrep -o '\S*'
}

strip_buildid () {
    echo "$1"|rev|cut -d. -f2-|rev
}

IDE_VERSION=$(get_version IDE com.st.stm32cube.common.mx)

MX_BUILD=$(get_version Artifact com.st.stm32cube.common.mx)
MX_VERSION=$(strip_buildid $MX_BUILD)

PRODUCTDB_BUILD=$(get_version Artifact com.st.stm32cube.ide.mcu.productdb.debug)
PRODUCTDB_VERSION=$(strip_buildid $PRODUCTDB_BUILD)

# May fail when the job is manually run
git tag -a "v-nightly-$TIMESTAMP" -m "Nightly auto-release with tree $(git rev-parse HEAD:)" || true

# Ignore failures in the following lines: We only create a tag for each version the first time we see it.
git tag -a "v-ide-$IDE_VERSION" -m "Auto-release for STM32 Cube IDE version $IDE_VERSION" || true

git tag -a "v-mx-$MX_BUILD" -m "Auto-release for STM32 Cube MX build $MX_BUILD" || true
git tag -a "v-mx-$MX_VERSION" -m "Auto-release for STM32 Cube MX version $MX_VERSION" || true

git tag -a "v-productdb-$PRODUCTDB_BUILD" -m "Auto-release for STM32 Product Database build $PRODUCTDB_BUILD" || true
git tag -a "v-productdb-$PRODUCTDB_VERSION" -m "Auto-release for STM32 Product Database version $PRODUCTDB_VERSION" || true

if [ ! -z ${GITLAB_ACCESS_TOKEN+x} ]; then
    echo "Access token found, pushing commit and tags"
    git push --tags --force https://gitlab-ci-token:${GITLAB_ACCESS_TOKEN}@gitlab.com/${CI_PROJECT_PATH} HEAD:release
else
    echo "No access token found. Not pushing anything."
fi
