from pathlib import Path
from bs4 import BeautifulSoup
import warnings
from collections import defaultdict
from contextlib import contextmanager
import hashlib
import re

def transform_family(family, family_dir, callable, ext):
    pn_variants = {}
    for sub_family in family.find_all('subFamily'):
        #print(f'  Sub-family {sub_family["name"]}')

        for device in sub_family.find_all('device'):
            # Make sure these error out if a device has no or multiple PN/variants sub-elements.
            pns, = device.find_all('PN')
            variants = device.find_all('variants')
            # This tag somehow contains a list of device names, one for each toolchan óÒ ?!
            pn = pns.string.split(',')[-1]

            assert len(variants) <= 1
            if variants:
                variants = variants[0].string.split(',')
            else:
                variants = ['default']
            
            if pn in pn_variants:
                if 'x' in pn: # generic part number pattern
                    continue

                if pn_variants[pn] != variants:
                    warnings.warn(f'Duplicate PN {pn} with mismatched variants!!')
                else:
                    warnings.warn(f'Duplicate PN {pn}!')

            pn_variants[pn] = variants

            with handle_variants(device, family_dir, pn, variants, ext) as chip_file:
                callable(device, chip_file)
        

def defrag_dir(le_dir, ext):
    le_map = defaultdict(lambda: [])
    le_reversed = {}
    contents = {}

    for f in le_dir.glob(f'*.{ext}'):
        if not f.is_file():
            continue
        if '_' in f.name:
            continue # special variant, see below
        
        pn = f.stem.replace('STM32', '')

        content = f.read_bytes()
        le_hash = hashlib.sha256(content).hexdigest()
        le_map[le_hash].append(pn)
        le_reversed[pn] = le_hash
        contents[le_hash] = content
    orig_reversed = le_reversed.copy()

    def gen_globs(pns):
        print('globbing', pns)
        for pn in pns:
            for i in range(len(pn)):
                glob = '^' + pn[:i] + '.' + pn[i+1:] + '$'
                
                our_matches = match_glob(glob, pns)
                global_matches = len(re.findall(glob, all_pns))
                if len(our_matches) > 1 and global_matches == len(our_matches):
                    print(glob, our_matches)
                    yield glob, our_matches

    def match_glob(glob, entries):
        return re.findall(glob, '\n'.join(entries), re.MULTILINE)

    for i in range(3):
        all_pns = '\n'.join(le_reversed.keys())
        next_reversed = le_reversed.copy()
        for pns in le_map.values():
            for glob, matches in gen_globs(pns):
                val = le_reversed[matches[0]]
                for match in matches:
                    if match in next_reversed:
                        del next_reversed[match]
                next_reversed[glob] = val

        next_map = defaultdict(lambda: [])
        for key, value in next_reversed.items():
            next_map[value].append(key)

        le_map, le_reversed = next_map, next_reversed

    for glob, le_hash in le_reversed.items():
        if '.' not in glob:
            continue # not a glob
    
        glob_file = le_dir / f'STM32{glob.replace(".", "x")}.{ext}'
        with glob_file.open('wb') as f:
            f.write(contents[le_hash])

    for pn in orig_reversed:
        for glob in le_reversed:
            if '.' not in glob:
                continue # not a glob

            if re.match(glob, pn):
                pn_file = le_dir / f'STM32{pn}.{ext}'
                glob_file = f'STM32{glob.replace(".", "x")}.{ext}'
                pn_file.unlink()
                pn_file.symlink_to(glob_file)
                break

def generate_chip_files(handlers):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infiles', nargs='*')
    parser.add_argument('-o', '--output-dir', default='.')
    args = parser.parse_args()

    outdir = Path(args.output_dir)
    outdir.mkdir(parents=True, exist_ok=False)

    for infile in args.infiles:
        with open(infile, 'r') as f:
            soup = BeautifulSoup(f.read(), 'xml')

        for family in soup.find_all('family'):
            family_dir = outdir / family['name'].replace('STM32', '')
            family_dir.mkdir(exist_ok=True)
            print(f'Family {family["name"]}')

            for ext, callable in handlers.items():
                transform_family(family, family_dir, callable, ext)

                print('Defragging directory...')
                defrag_dir(family_dir, ext=ext)

@contextmanager
def handle_variants(device, sub_family_dir, pn, variants, ext):
    pn_file = sub_family_dir / f'{pn}.{ext}'

    if pn_file.exists():
        bak_file = pn_file.with_suffix(f'.{ext}.bak')
        pn_file.rename(bak_file)
    else: bak_file = None

    #print(f'    Writing {pn}')
    with pn_file.open('w') as f:
        yield f

    # There are some strange cases where there are two entries for the same part number, that have different
    # memory maps and are only distinguished by their "<variant>". We use the first one as the default and
    # save the second one's memory map in a file named after the variant STM32...._<variant>.ldi
    variant_suffix = ''
    if bak_file:
        if bak_file.read_text() == pn_file.read_text():
            bak_file.unlink()
        else:
            variant_suffix = f'_{",".join(variants)}'
            tmp = pn_file
            pn_file = pn_file.rename( sub_family_dir / f'{pn}{variant_suffix}.{ext}' )
            bak_file.rename(tmp)

