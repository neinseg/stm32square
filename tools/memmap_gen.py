#!/usr/bin/env python3

import utils

def gen_memmap(device, f):
    for memory in device.find_all('memory', recursive=True):
        name = memory["name"]
        access = memory["access"]
        origin = int(memory["start"], base=16)
        size = memory["size"]
        print(f'{name:<12} ({access:<3}): ORIGIN = 0x{origin:08x}, LENGTH = {size}K', file=f)

if __name__ == '__main__':
    utils.generate_chip_files({'ldi': gen_memmap})


