STM32 Support Files Git Repository
==================================

This is an inofficial git repository containing data files describing ST's STM32 series of microcontrollers. It is
automatically updated every night from ST's STM32 Cube IDE Eclipes P2 update servers. The data files in this repo
contain machine-readable descriptions of e.g. the microcontrollers memory map (.db files) and peripherals (svd).

The "official" inofficial home of this repo is: https://gitlab.com/neinseg/stm32square

Raison d'être
-------------

ST's tooling such as the free STM32 Cube IDE as well as their STM32 Cube driver/BSP packages contain a lot of
machine-readable information about STM32 microcontroller's internals. There's the ARM standard SVD files that describe
all peripheral registers and their bits, and there's a whole lot of ST-specific XML files that describe things such as
the chip's memory map.

Both of these are extremely useful when you're building your STM32 firmware outside of ST's STM32 Cube IDE. Since the
Cube IDE is Eclipse-based, there are about 2.2GB worth of good reasons to use another toolchain.

ST already publishes the driver and template code packages `on github
<https://github.com/STMicroelectronics/STM32Cube_MCU_Overall_Offer>`_. These repositories contain things like startup
code and CMSIS headers for all their microcontrollers, but for some reason ST does not include SVD files or
machine-readable memory maps anywhere.

For this reason, in its "Release" branch, this repository contains a copy of the data files used by the STM32 Cube IDE
that is updated daily using a Gitlab CI. The CI job pulls ST's official STM32 Cube IDE Eclipse artifacts from the IDE's
Eclipse p2 update repository and extracts the relevant data files from these Eclipse artifacts.

Licensing
---------

I was pleasantly surprised to see that ST has included fairy permissive licenses with all of these files.  All artifacts
contain license information either right next to the data files or as part of the artifact. We simply copy this license
information along with the data files. Please read these licenses for yourself, and please check for yourself that they
apply. We cannot guarantee that the information in this repo is correct. The main thing you should be aware of is that
in some of their licenses, ST prohibits usage of their stuff with non-ST chips. Notably, this would prevent you from
using it with any of the gazillion chinese clones of their popular F103 µC.

The Makefile, shell and python CI code itself in this repo is licensed under a `Creative Commons Zero (CC-0) public
domain license <http://creativecommons.org/publicdomain/zero/1.0/>`_.

Repository Structure
--------------------

Branches
~~~~~~~~

This repository has two main branches: The master branch contains only the pristine update scripts, and does not contain
any pulled files anywhere in its history. The release branch merges in any changes to the update scripts on the master
branch, but additionally is updated by the nightly gitlab CI run to include ST's data files.

To use these files from your code, you could either pull in this repo's ``release`` branch e.g. as a git submodule, or
you could pull in its ``master`` branch and use the ``Makefile`` to let ``make`` download the files for you. 

Tags
~~~~

The nightly CI job automatically generates three types of tags:

``v-nightly-20-11-18``
  Nightly releases. One such release is generated each day, and contains ST's latest update.

``v-ide-1.2.3``
  STM32 Cube IDE release. Every time the IDE updates to a new version, a tag like this is created.

This repo extracts data from three separate STM32 Cube IDE Eclipse P2 Artifacts that are versioned independently by ST.
For every new version of each of these artifacts, two tags are created as follows:

``v-[component]-1.2.3``
  Component version tag. This describes the state when the artifact version ``1.2.3`` was published.

``v-[component]-1.2.3.202011182342``
  Component build tag. This describes the state when the artifact build ``202011182342`` of version ``1.2.3`` was published.

Building
--------

One thing to note is that ST's Eclipse P2 repo server seems to kind of suck. Requests time out a lot, which is why we
have wget retry up to 10 times. In case you're still getting download errors, be aware that chances are it's their
server and not your internet.

