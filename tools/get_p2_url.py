#!/usr/bin/env python3
import re
import sys
import lzma
from urllib.parse import urlparse
from contextlib import suppress
import os
from os import path
import shutil
import time

import requests
from bs4 import BeautifulSoup

def fucked_up_get(*args, **kwargs):
    for retry in range(5):
        for timeout in [0.1, 0.5, 1.0, 2.0, 5.0]:
            kwargs.update({'timeout': timeout})
            try:
                return requests.get(*args, **kwargs)
            except requests.exceptions.Timeout:
                pass
        time.sleep(0.5)
    raise requests.exceptions.Timeout()

def crop(s, length=80):
    if len(s) <= length:
        return s + (' ' * (length-len(s)))

    if length > 40:
        return s[:length - 20 - 3] + '...' + s[-20:]

    return s[:length]

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--update-site', default='http://sw-center.st.com/stm32cubeide/updatesite1')
    parser.add_argument('artifact_id')
    parser.add_argument('-v', '--version', action='store_true')
    parser.add_argument('--write-version', type=argparse.FileType('w'))
    args = parser.parse_args()

    if args.write_version:
        args.version = True
        version_file = args.write_version
    else:
        version_file = sys.stdout

    if args.version:
        print(f'[Artifact {args.artifact_id}]', file=version_file)

    res = fucked_up_get(f'{args.update_site}/compositeContent.xml')
    res.raise_for_status()
    soup = BeautifulSoup(res.content, features='xml')

    versions = []
    for child in soup.find('repository').find('children').find_all('child'):
        loc = child['location'].split('.')
        try:
            versions.append(tuple(int(elem) for elem in loc))
        except:
            pass
    latest_version = '.'.join(map(str, sorted(versions)[-1]))

    if args.version:
        print('IDE version:', latest_version, file=version_file)

    res = fucked_up_get(f'{args.update_site}/{latest_version}/artifacts.xml.xz')
    res.raise_for_status()
    soup = BeautifulSoup(lzma.LZMADecompressor().decompress(res.content), features='xml')

    artifact = soup.find('artifact', recursive=True, id=args.artifact_id)
    art_id, art_ver, art_cls = artifact['id'], artifact['version'], artifact['classifier']
    if args.version:
        print('Artifact version:', artifact['version'], file=version_file)

    filename = f'{art_id}_{art_ver}.jar'
    url = f'{args.update_site}/{latest_version}/plugins/{filename}'

    if not args.version or args.write_version:
        print(f'{url}')

